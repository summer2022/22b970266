PV = "1.5.7"

SRC_URI += " \
    file://bugfix-cronie-systemd-alias.patch \
"

SRC_URI[md5sum] = "544f141aa4e34e0a176529be08441756"
SRC_URI[sha256sum] = "538bcfaf2e986e5ae1edf6d1472a77ea8271d6a9005aee2497a9ed6e13320eb3"
